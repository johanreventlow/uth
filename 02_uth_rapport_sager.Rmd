---
title: 'Utilsigtede hændelser i `r afd`'
subtitle: | 
  | 2\. delrapport: Detaljerede sagsoplysninger 
  | \
  | `r format(Sys.Date()-months(1), "%B %Y")`
author: 'Udarbejdet af Afdeling for Kvalitet og Uddannelse, Bispebjerg og Frederiksberg Hospital'
date: '`r format(Sys.Date(), "%d. %B %Y")`'
output:
  word_document: 
    reference_docx: 'uth_rapportskabelon.docx'
    fig_caption: true
  pdf_document: default
  html_document: default
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=FALSE)
hosp_blaa <-
  rgb(
  red = 0,
  green = 156 / 255,
  blue = 232 / 255,
  names = "hosp_blue",
  alpha = 1
  )

# '2. delrapport: Detaljerede sagsoplysninger'
  seneste_maaned_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - months(1)
  seneste_maaned_slut_dato <- as.Date(cut(Sys.Date(), 'month'))
  seneste_aar_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - months(12)
  seneste_aar_slut_dato <- as.Date(cut(Sys.Date(), 'month'))
  seneste_90dage_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - days(90)
  seneste_90dage_slut_dato <- as.Date(cut(Sys.Date(), 'month'))
  udloeber_start_dato <- as.Date(cut(as.Date(Sys.Date()) %m+% months(1), 'month')) - 91

  # Opsummeringsdata
  
  Opsummeringsdata <- d %>%
  filter(!duplicated(Sagsnummer) &
  Afdeling.y == afd &
  ((
  Opret_dato >= seneste_aar_start_dato &
  Opret_dato < seneste_aar_slut_dato
  ) | Sagsstatus != "Lukket"
  ))
  
  Opsummeringsdata <- as_tibble(lapply(Opsummeringsdata, tilfoejTom))
  
  Opsummeringsdata$Alvorlighed[is.na(Opsummeringsdata$Alvorlighed)] <-
  "(ikke angivet)"
  Opsummeringsdata$DPSD_Hovedgruppe[is.na(Opsummeringsdata$DPSD_Hovedgruppe)] <-
  "(ikke angivet)"
  
  Opsummeringsdata$DPSD_Proces[is.na(Opsummeringsdata$DPSD_Proces)] <-
  "(ikke angivet)"
  
  Opsummeringsdata$DPSD_Problem[is.na(Opsummeringsdata$DPSD_Problem)] <-
  "(ikke angivet)"
  
  
  # Enkeltsager, forberedelse af data
  
  forberedte_data <- as_tibble(d) %>%
  filter(!duplicated(Sagsnummer)) %>%
  select(
  Sagsnummer,
  Sagsstatus,
  Sagstype,
  `Hændelsesdato`,
  `Hændelsestid` = `Hændelsetid`,
  `Hændelsessted`,
  Involveret_lokationsnavn,
  Opret_dato,
  Sagsafslutning_dato,
  Afdeling,
  Afdeling.y,
  Alvorlighed,
  DPSD_Hovedgruppe,
  DPSD_Proces,
  DPSD_Problem,
  `Hændelsesbeskrivelse`,
  Konsekvens,
  Forslag_til_forebyggelse,
  `Beskrivelse_af_sagsopfølgning`,
  Titel
  ) %>%
  arrange(`Hændelsessted`)
  
  forberedte_data$`Hændelsesdato` <-
  format(as.Date(forberedte_data$`Hændelsesdato`), "%d. %B %Y")
  forberedte_data$`Opret_dato` <-
  format(as.Date(forberedte_data$`Opret_dato`), "%d. %B %Y")
  #forberedte_data$`Afsluttet_dato` <-
  #format(as.Date(forberedte_data$`Afsluttet_dato`), "%d. %B %Y")
  forberedte_data$`Sagsafslutning_dato` <-
  format(as.Date(forberedte_data$`Sagsafslutning_dato`), "%d. %B %Y")
  
  forberedte_data$`Hændelsestid` <-
  format(strptime(forberedte_data$`Hændelsestid`, "%H:%M"), "%H:%M")
  
  forberedte_data$`Hændelsessted` <-
  gsub(
  'Region Hovedstaden Bispebjerg og Frederiksberg Hospitaler |Region Hovedstaden Bispebjerg Hospital ',
  '',
  forberedte_data$`Hændelsessted`
  )
  forberedte_data$`Hændelsessted` <-
  str_replace_all(forberedte_data$`Hændelsessted`,
  '[[~!@#$%^&*(){}\\ +:<>?./;=]]',
  ' ')
  
  forberedte_data <- as_tibble(lapply(forberedte_data, tilfoejTom))
  forberedte_data[is.na(forberedte_data)] <- "(ikke angivet)"
  
  #åbne sager
  out_aabne <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Åben", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
 
   #Videresendte sager
  out_videresendte <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Videresendt", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #Accepterede sager
  out_accepterede <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Accepteret", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #Oprettede sager
  out_oprettede <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Oprettet", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #lukkede sager
  out_lukkede_sager <- forberedte_data %>%
  filter(
  Afdeling.y == afd,
  Sagsstatus == "Lukket",
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") >= seneste_maaned_start_dato,
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") < seneste_maaned_slut_dato
  )
  
  #Sager ældre end 90 dage
  out_aeldre_sager <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus != "Lukket", as.Date(Opret_dato, format = "%d. %B %Y") < as.Date(seneste_90dage_start_dato))
  
  #Sager der snart overskrider 90 dage
  out_udloeber_sager <- forberedte_data %>%
  filter(Afdeling.y == afd, 
         Sagsstatus != "Lukket", 
         as.Date(Opret_dato, format = "%d. %B %Y") < as.Date(udloeber_start_dato),
         as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato)
         )
  
  #forslag til forebyggelse
  out_forslag_til_forebyggelse <- forberedte_data %>%
  filter(
  Sagsstatus == "Lukket",
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") >= seneste_maaned_start_dato,
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") < seneste_maaned_slut_dato
  ) %>%
  select(DPSD_Hovedgruppe, 
  Titel,
  Forslag_til_forebyggelse)
  
  #involveret lokation
  involveret <- d %>%
  filter(
  !duplicated(Sagsnummer) &
  (
  Opret_dato >= seneste_aar_start_dato &
  Opret_dato < seneste_aar_slut_dato
  ) & Sagsstatus == "Lukket" & Sagstype == "Sundhedsfaglig UTH 2"
  )
  
```

```{r Wordcloud, echo=FALSE, fig.height=7, fig.width=7, message=FALSE, warning=FALSE, dpi = 300}

Afd_words_this <- Afd_words %>%
  filter(Afdeling.y == afd) %>%
  top_n(100)
  
  wordcloud::wordcloud(
  Afd_words_this$word,
  freq = Afd_words_this$freq,
  max.words = 100,
  scale = c(4, .7),
  use.r.layout = FALSE,
  colors = hosp_blaa,
  rot.per = .3,
  random.order = FALSE
  )
  

```


# Forord
Dette er 2. delrapport ud af 2 for utilsigtede hændelser i `r format(Sys.Date()-months(1), "%B %Y")` i `r afd`.
Rapporten har følgende opbygning:

```{r forord, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
if (count(out_oprettede) > 0) cat(paste('* Oprettede sager', ' (', count(out_oprettede), ' stk.)','\n', sep=""))

if (count(out_videresendte) > 0) cat(paste('* Videresendte sager', ' (', count(out_videresendte), ' stk.)','\n', sep=""))

if (count(out_accepterede) > 0) cat(paste('* Accepterede sager', ' (', count(out_accepterede), ' stk.)','\n', sep=""))

if (count(out_aabne) > 0) cat(paste('* Åbne sager', ' (', count(out_aabne), ' stk.)','\n', sep=""))

if (count(out_lukkede_sager) > 0) cat(paste('* Lukkede sager i ', format(seneste_maaned_start_dato, "%B %Y"), ' (', count(out_lukkede_sager), ' stk.)','\n', sep=""))

if (count(out_udloeber_sager) > 0) cat(paste('* Uafsluttede sager, hvor sagsbehandlingstiden vil overskride 90 dage i ', format(seneste_90dage_slut_dato %m+% months(1) -1, "%B %Y"),' (', count(out_udloeber_sager), ' stk. pr.', format(seneste_90dage_slut_dato, "%e. %B %Y"), ')','\n', sep=""))

if (count(out_aeldre_sager) > 0) cat(paste('* Uafsluttede sager, som er ældre end 90 dage (', count(out_aeldre_sager), ' stk. pr.', format(seneste_90dage_slut_dato, "%e. %B %Y"), ')','\n', sep=""))

antal_sager <- sum(c(count(out_oprettede), count(out_videresendte), count(out_accepterede), count(out_aabne), count(out_lukkede_sager)) == 0)

inaktive_kapitler <- paste(c(if (count(out_oprettede) == 0) "'oprettede sager'", if (count(out_videresendte) == 0) "'videresendte sager'", if (count(out_accepterede) == 0) "'accepterede sager'", if (count(out_aabne) == 0) "'åbne sager'", if (count(out_lukkede_sager) == 0) "'lukkede sager'"), collapse = ", ")

inaktive_kapitler <- gsub("(.*)\\,(.*)", "\\1 eller\\2", inaktive_kapitler)

if (antal_sager > 0) cat(paste('\n\n Der var ikke sager i kategori', ifelse(antal_sager > 1, 'erne ', 'en '), inaktive_kapitler, ", og kapitl", ifelse(antal_sager > 1, 'erne ', 'et '), 'udelades derfor.', sep="" ) )

```

Forsideillustrationen er dannet af de 100 vigtigste ord i hændelsesbeskrivelserne fra `r afd` i `r format(seneste_aar_slut_dato - months(1), "%B %Y")`. 

Tip til læsning på skærm: Vælg "Vis" i MS Word, og sæt hak ved "Navigationsrude" for en navigerbar indholdsfortegnelse.  

Tip til læsning på papir: Vælg "Referencer" i MS Word før udskrivning, og indsæt "Indholdsfortegnelse". "Automatisk Tabel 1" og "Automatisk Tabel 2" er lige gode. Desværre er det af tekniske årsager ikke muligt at generere indholdsfortegnelsen når rapporten dannes.

Data til denne rapport er trukket fra DPSD2 den `r format(data_trukket, "%e. %B %Y")`.

Spørgsmål, kommentarer, idéer og ønsker til forbedring af denne rapport modtages gerne, og bedes sendt til johan.reventlow@regionh.dk 

  
  
  

Mia Hartley, Risikomanager  
  
Afdeling for Kvalitet og Uddannelse  
Bispebjerg og Frederiksberg Hospital

`r format(Sys.Date(), '%e %B %Y')`


```{r kable_oprettede, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

# OPRETTEDE SAGER

if (count(out_oprettede) > 0) cat(paste('# Oprettede sager', ' (', count(out_oprettede), ' stk.)','\n','\n', sep=""))

out <- out_oprettede %>%   
  arrange(DPSD_Hovedgruppe, Opret_dato)

names(out) <- gsub('(_)','\\ ',names(out))

for (j in unique(out$`Hændelsessted`)){
  cat('## ', j)
  cat('\n')
  sted <- out %>% 
    filter(`Hændelsessted` == j)
  for (k in unique(sted$`DPSD Hovedgruppe`)){
    gruppe <- sted %>% 
      filter(`DPSD Hovedgruppe` == k)
    cat('### ', k)
    cat('\n')
    for (i in seq(as.character(gruppe$Sagsnummer))) {
      cat('#### ', gruppe[i, ]$Titel)
      gather(select(gruppe, -Titel, -Afdeling.y)[i, ], Felt, `Værdi`) %>%
        kable() %>%
        print()
      cat('\n')
    }
  }
}

```

```{r kable_videresendte, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
# VIDERESENDTE SAGER

if (count(out_videresendte) > 0) cat(paste('# Videresendte sager', ' (', count(out_videresendte), ' stk.)','\n','\n', sep=""))

out <- out_videresendte %>% 
    arrange(DPSD_Hovedgruppe, Opret_dato)

names(out) <- gsub('(_)','\\ ',names(out))

for (j in unique(out$`Hændelsessted`)){
  cat('## ', j)
  cat('\n')
  sted <- out %>% 
    filter(`Hændelsessted` == j)
  for (k in unique(sted$`DPSD Hovedgruppe`)){
    gruppe <- sted %>% 
      filter(`DPSD Hovedgruppe` == k)
    cat('### ', k)
    cat('\n')
    for (i in seq(as.character(gruppe$Sagsnummer))) {
      cat('#### ', gruppe[i, ]$Titel)
      gather(select(gruppe, -Titel, -Afdeling.y)[i, ], Felt, `Værdi`) %>%
        kable() %>%
        print()
      cat('\n')
    }
  }
}


```

```{r kable_accepterede, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

# ACCEPTEREDE SAGER

if (count(out_accepterede) > 0) cat(paste('# Accepterede sager', ' (', count(out_accepterede), ' stk.)','\n','\n', sep=""))

out <- out_accepterede %>% 
    arrange(DPSD_Hovedgruppe, Opret_dato)

names(out) <- gsub('(_)','\\ ',names(out))

for (j in unique(out$`Hændelsessted`)){
  cat('## ', j)
  cat('\n')
  sted <- out %>% 
    filter(`Hændelsessted` == j)
  for (k in unique(sted$`DPSD Hovedgruppe`)){
    gruppe <- sted %>% 
      filter(`DPSD Hovedgruppe` == k)
    cat('### ', k)
    cat('\n')
    for (i in seq(as.character(gruppe$Sagsnummer))) {
      cat('#### ', gruppe[i, ]$Titel)
      gather(select(gruppe, -Titel, -Afdeling.y)[i, ], Felt, `Værdi`) %>%
        kable() %>%
        print()
      cat('\n')
    }
  }
}


```

```{r kable_aabne, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
# ÅBNE SAGER

if (count(out_aabne) > 0) cat(paste('# Åbne sager', ' (', count(out_aabne), ' stk.)','\n','\n', sep=""))

out <- out_aabne %>% 
  arrange(DPSD_Hovedgruppe, Opret_dato)

names(out) <- gsub('(_)','\\ ',names(out))

for (j in unique(out$`Hændelsessted`)){
  cat('## ', j)
  cat('\n')
  sted <- out %>% 
    filter(`Hændelsessted` == j)
  for (k in unique(sted$`DPSD Hovedgruppe`)){
    gruppe <- sted %>% 
      filter(`DPSD Hovedgruppe` == k)
    cat('### ', k)
    cat('\n')
    for (i in seq(as.character(gruppe$Sagsnummer))) {
      cat('#### ', gruppe[i, ]$Titel)
      gather(select(gruppe, -Titel, -Afdeling.y)[i, ], Felt, `Værdi`) %>%
        kable() %>%
        print()
      cat('\n')
    }
  }
}

```


```{r kable_lukkede, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
#LUKKEDE SAGER

if (count(out_lukkede_sager) > 0) cat(paste('# Lukkede sager i ', format(seneste_maaned_start_dato, "%B %Y"), ' (', count(out_lukkede_sager), ' stk.)','\n','\n', sep=""))


out <- out_lukkede_sager %>% 
    arrange(DPSD_Hovedgruppe, Opret_dato)

names(out) <- gsub('(_)','\\ ',names(out))

for (j in unique(out$`Hændelsessted`)){
  cat('## ', j)
  cat('\n')
  sted <- out %>% 
    filter(`Hændelsessted` == j)
  for (k in unique(sted$`DPSD Hovedgruppe`)){
    gruppe <- sted %>% 
      filter(`DPSD Hovedgruppe` == k)
    cat('### ', k)
    cat('\n')
    for (i in seq(as.character(gruppe$Sagsnummer))) {
      cat('#### ', gruppe[i, ]$Titel)
      gather(select(gruppe, -Titel, -Afdeling.y)[i, ], Felt, `Værdi`) %>%
        kable() %>%
        print()
      cat('\n')
    }
  }
}


```

```{r kable_udloeb, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
# SAGER DER SNART UDLØBER

if (count(out_udloeber_sager) > 0) cat(paste('# Uafsluttede sager, hvor sagsbehandlingstiden vil overskride 90 dage i ', format(seneste_90dage_slut_dato %m+% months(1) -1, "%B %Y"), ' (', count(out_udloeber_sager), ' stk.)','\n','\n', sep=""))

out <- out_udloeber_sager %>% 
  arrange(as.Date(Opret_dato, format="%d. %B %Y"))

names(out) <- gsub('(_)','\\ ',names(out))


if (count(out_udloeber_sager) > 0) out %>% select(Sagsnummer, `Opret dato`) %>% 
  mutate(Sagsnummer = as.character(Sagsnummer), Sagsbehandlingsfrist = format(as.Date(`Opret dato`, format="%d. %B %Y")+90, "%d. %B %Y")) %>% 
kable() %>% print()

    

```

```{r kable_aeldre, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}
# ÆLDRE SAGER

if (count(out_aeldre_sager) > 0) cat(paste('# Uafsluttede sager, der er ældre end 90 dage (', count(out_aeldre_sager), ' stk. pr.', format(seneste_90dage_slut_dato, "%e. %B %Y"), ')','\n','\n', sep=""))

out <- out_aeldre_sager %>% 
  arrange(as.Date(Opret_dato, format="%d. %B %Y"))

names(out) <- gsub('(_)','\\ ',names(out))

if (count(out_aeldre_sager) > 0) out %>% mutate(Sagsnummer = as.character(Sagsnummer)) %>% 
  select(Sagsnummer, `Opret dato`) %>%
kable() %>% print()

    

```
