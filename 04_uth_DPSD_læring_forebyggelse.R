library(tidyverse)
library(lubridate)
library(knitr)
library(pander)
library(stringr)

# Gør klar ----------------------------------------------------------------

rm(list = ls())
raadata <- "V:/BYH/Datadrevet ledelse/2_RÅDATA/uth/"
skabeloner <- "C:/Users/jrev0004/Documents/Synkronisering/Dropbox/Bispebjerg Data- og Kvalitetskonsulent/R/uth/"
rapporter <- "V:/BYH/Datadrevet ledelse/2_RÅDATA/uth/output/"



setwd(raadata)

script <- 'Option Explicit

Dim fso, wd, fol , f, ext, oDoc, tbl
Set fso = WScript.CreateObject("Scripting.FileSystemObject")
Set wd = CreateObject("Word.Application")
set fol = fso.GetFolder(fso.GetParentFolderName(WScript.ScriptFullName))

On Error Resume Next

For Each f In fol.Files
ext = LCase(fso.GetExtensionName(f.Name))

If ext = "doc" Or ext = "docx" Then

Set oDoc = wd.Documents.Open(f.Path)

For Each tbl In oDoc.Tables
If tbl.Columns.Count = 4 Then
tbl.AllowAutoFit = False
tbl.Columns(1).Width = wd.Application.CentimetersToPoints(3)
tbl.Columns(2).Width = wd.Application.CentimetersToPoints(3)
tbl.Columns(3).Width = wd.Application.CentimetersToPoints(4)
tbl.Columns(4).Width = wd.Application.CentimetersToPoints(6.2)
End If

If tbl.Columns.Count = 3 Then
tbl.AllowAutoFit = False
tbl.Columns(1).Width = wd.Application.CentimetersToPoints(3.4)
tbl.Columns(2).Width = wd.Application.CentimetersToPoints(6.4)
tbl.Columns(3).Width = wd.Application.CentimetersToPoints(6.4)
End If

If tbl.Columns.Count = 2 Then
tbl.AllowAutoFit = False
tbl.Columns(1).Width = wd.Application.CentimetersToPoints(4)
tbl.Columns(2).Width = wd.Application.CentimetersToPoints(12.2)
End If

tbl.Rows(1).HeadingFormat = True

Next

set wdRange = oDoc.Range(1,1)
oDoc.TablesOfContents.Add(wdRange)

oDoc.Save
oDoc.Close
End If

Next
'
writeLines(script, paste(rapporter, "fix_table_sizes.vbs", sep = ""))


# script <- 'Option Explicit
#
# Dim fso, wd, fol , f, ext, oDoc, tbl
# Set fso = WScript.CreateObject("Scripting.FileSystemObject")
# Set wd = CreateObject("Word.Application")
# set fol = fso.GetFolder(fso.GetParentFolderName(WScript.ScriptFullName))
#
# On Error Resume Next
#
# For Each f In fol.Files
# ext = LCase(fso.GetExtensionName(f.Name))
#
# If ext = "doc" Or ext = "docx" Then
#
# Set oDoc = wd.Documents.Open(f.Path)
#
#
#
#
#
# oDoc.Save
# oDoc.Close
# End If
#
# Next
# '
# writeLines(script, paste(rapporter, "fix_table_sizes.vbs", sep = ""))

# Indlæs data -------------------------------------------------------------
#
# load("uth_datasæt.RData")
#
# # Funktioner --------------------------------------------------------------
#
# tilfoejTom <- function(x){
#   if(is.factor(x)) return(factor(x, levels=c(levels(x), "(tom)", "(ikke angivet)")))
#   return(x)
# }
#
# # # Tidytext Beregninger -------------------------------------------------------------
# #
# Problem_words <- d %>%
#   filter(DPSD_Problem != "", as.Date(Opret_dato) >= as.Date("2013-01-01")) %>%
#   tidytext::unnest_tokens(word, Forslag_til_forebyggelse) %>%
#   anti_join(stop_ord) %>%
#   count(DPSD_Hovedgruppe, DPSD_Proces, DPSD_Problem, word, sort=TRUE) %>%
#   ungroup()
#
# total_words <- Problem_words %>% group_by(DPSD_Problem) %>%
#   summarize(total = sum(n))
#
# Problem_words <- left_join(Problem_words, total_words)
#
# Problem_words <- anti_join(Problem_words, stop_ord)
#
# Problem_words <- Problem_words %>%
#   tidytext::bind_tf_idf(word, DPSD_Hovedgruppe, n) %>%
#   mutate(freq =  tf_idf * n)
# #
# #
# # Alvor_words <- d %>%
# #   filter(Alvorlighed != "", as.Date(Opret_dato) >= (as.Date(cut(Sys.Date(), "month"))-months(1))) %>%
# #   tidytext::unnest_tokens(word, text) %>%
# #   anti_join(stop_ord) %>%
# #   count(Alvorlighed, word, sort=TRUE) %>%
# #   ungroup()
# #
# # total_words <- Alvor_words %>% group_by(Alvorlighed) %>%
# #   summarize(total = sum(n))
# #
# # Alvor_words <- left_join(Alvor_words, total_words)
# #
# # Alvor_words <- anti_join(Alvor_words, stop_ord)
# #
# # Alvor_words <- Alvor_words %>%
# #   tidytext::bind_tf_idf(word, Alvorlighed, n) %>%
# #   mutate(freq =  tf_idf * n)
# #
# #
# # # Afd_words <- d %>%
# # #   filter(as.Date(Opret_dato) >= (as.Date(cut(Sys.Date(), "month"))-months(1))) %>%
# # #   tidytext::unnest_tokens(word, text) %>%
# # #   anti_join(stop_ord) %>%
# # #   count(Afdeling.y, word, sort=TRUE) %>%
# # #   ungroup()
# #
# # Afd_words <- d %>%
# #   filter((
# #     Sagsstatus != "Lukket" &
# #       as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(cut(Sys.Date(), 'month')) - days(90)
# #   ) |
# #     (
# #       Sagsstatus == "Lukket" &
# #         as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") >= (as.Date(cut(Sys.Date(
# #         ), "month")) - months(1))
# #     )) %>%
# #   tidytext::unnest_tokens(word, text) %>%
# #   anti_join(stop_ord) %>%
# #   count(Afdeling.y, word, sort = TRUE) %>%
# #   ungroup()
# #
# # total_words <- Afd_words %>% group_by(Afdeling.y) %>%
# #   summarize(total = sum(n))
# #
# # Afd_words <- left_join(Afd_words, total_words)
# #
# # Afd_words <- anti_join(Afd_words, stop_ord)
# #
# # Afd_words <- Afd_words %>%
# #   tidytext::bind_tf_idf(word, Afdeling.y, n) %>%
# #   mutate(freq =  tf_idf * n)
#
#
# # Skriv rapporter ---------------------------------------------------------
#
# # d$Afdeling.y[d$Afdeling.y == "INDGÅR IKKE"] <- "Udefunktioner"
# # Afd_words$Afdeling.y[Afd_words$Afdeling.y == "INDGÅR IKKE"] <- "Udefunktioner"
# #
# # afdelinger <- d %>%
# #   filter(Sagsstatus != "Lukket", as.Date(Opret_dato, format = "%d. %B %Y") >= as.Date(as.Date(cut(Sys.Date(), 'month')) - days(90)))
# #
# # afdelinger <- unique(afdelinger$Afdeling.y[afdelinger$Sagsstatus != "Lukket"])
# #
# # afdelinger <- sort(afdelinger[!is.na(afdelinger)])
#
#
# forslag <- d %>%
#   filter(DPSD_Problem != "", as.Date(Opret_dato) >= as.Date("2013-01-01")) %>%
#   select(DPSD_Hovedgruppe, DPSD_Proces, DPSD_Problem, Titel, Forslag_til_forebyggelse, Beskrivelse_af_sagsopfølgning)
#
# #### Statistik og organisatorisk læring
#
# d <- as_tibble(lapply(d, tilfoejTom))
#
#
# d$Alvorlighed[is.na(d$Alvorlighed)] <-
#   "(ikke angivet)"
#
# d$DPSD_Hovedgruppe[is.na(d$DPSD_Hovedgruppe)] <-
#   "(ikke angivet)"
#
# d$DPSD_Proces[is.na(d$DPSD_Proces)] <-
#   "(ikke angivet)"
#
# d$DPSD_Problem[is.na(d$DPSD_Problem)] <-
#   "(ikke angivet)"
#
#
# e <- d %>%
#   filter(!is.na(DPSD_Hovedgruppe), !is.na(DPSD_Proces), !is.na(DPSD_Problem), Sagsstatus == "Lukket")
#
# dir.create(file.path(raadata, "DPSD"))
# for (i in sort(unique(e$DPSD_Hovedgruppe))) {
#   dir.create(file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i)))
#   hovedgruppe <- e %>%
#     filter(DPSD_Hovedgruppe == i)
#   for(j in sort(unique(hovedgruppe$DPSD_Proces))) {
#     dir.create(file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i), gsub("\\/|\\s\\s+", " ", j)))
#     proces <- hovedgruppe %>%
#       filter(DPSD_Proces == j)
#     for(k in sort(unique(proces$DPSD_Problem))) {
#       #   dir.create(file.path(raadata, "DPSD", i,j, k))
#       # cat(paste('DPSD', gsub("\\s\\s+", " ",i), j, k, sep="\\"), '\n')
#       rmarkdown::render(paste(skabeloner, '01_uth_rapport_DPSD.Rmd', sep = ""),
#                         output_file =  paste(str_replace_all(k, '[[~!@#$%^&*{}\\ +:<>?,./;=]]', '_'), ".docx", sep=''),
#                         output_dir = file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i), gsub("\\/|\\s\\s+", " ", j)),
#                         encoding='UTF-8')
#     }
#
#     writeLines(script, paste(file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i), gsub("\\/|\\s\\s+", " ", j)), "/fix_table_sizes.vbs", sep = ""))
#     shell.exec( paste(file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i), gsub("\\/|\\s\\s+", " ", j)), "/fix_table_sizes.vbs", sep = ""))
#
#     Sys.sleep(3)
#
#     file.remove( paste(file.path(raadata, "DPSD", gsub("\\/|\\s\\s+", " ", i), gsub("\\/|\\s\\s+", " ", j)), "/fix_table_sizes.vbs", sep = ""))
#
#
#   }
# }
#
#
#
#
#
#
# i <- "Medicinering herunder væsker"
# j <- "Ordination, receptkontrol"
# k <- "Andet"
#
#
#
#
#
#
# # Ryd op ------------------------------------------------------------------
#
shell.exec( paste(rapporter, "fix_table_sizes.vbs", sep = ""))
# #
# # Sys.sleep(3)
# #
# # file.remove( paste(rapporter, "fix_table_sizes.vbs", sep = ""))
#
# this.dir <- dirname(parent.frame(2)$ofile)
# setwd(this.dir)
# rm(list = ls())
# .rs.restartR()
