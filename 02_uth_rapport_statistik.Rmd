---
title: 'Utilsigtede hændelser i `r afd`'
subtitle: | 
  | 1\. delrapport: Statistik og 
  | organisatorisk læring
  | \
  | `r format(Sys.Date()-months(1), "%B %Y")`
author: 'Udarbejdet af Afdeling for Kvalitet og Uddannelse, Bispebjerg og Frederiksberg Hospital'
date: '`r format(Sys.Date(), "%d. %B %Y")`'
output:
  word_document: 
    reference_docx: 'uth_rapportskabelon.docx'
    fig_caption: true
  pdf_document: default
  html_document: default
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=FALSE)
hosp_blaa <-
  rgb(
  red = 0,
  green = 156 / 255,
  blue = 232 / 255,
  names = "hosp_blue",
  alpha = 1
  )

# '2. delrapport: Detaljerede sagsoplysninger'
  seneste_maaned_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - months(1)
  seneste_maaned_slut_dato <- as.Date(cut(Sys.Date(), 'month'))
  seneste_aar_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - months(12)
  seneste_aar_slut_dato <- as.Date(cut(Sys.Date(), 'month'))
  seneste_90dage_start_dato <-
  as.Date(cut(Sys.Date(), 'month')) - days(90)
  seneste_90dage_slut_dato <- as.Date(cut(Sys.Date(), 'month'))

  
  # Opsummeringsdata
  
  Opsummeringsdata <- d %>%
  filter(!duplicated(Sagsnummer) &
  Afdeling.y == afd &
  ((
  Opret_dato >= seneste_aar_start_dato &
  Opret_dato < seneste_aar_slut_dato
  ) | Sagsstatus != "Lukket"
  ))
  
  Opsummeringsdata <- as_tibble(lapply(Opsummeringsdata, tilfoejTom))
  
  Opsummeringsdata$Alvorlighed[is.na(Opsummeringsdata$Alvorlighed)] <-
  "(ikke angivet)"
  Opsummeringsdata$DPSD_Hovedgruppe[is.na(Opsummeringsdata$DPSD_Hovedgruppe)] <-
  "(ikke angivet)"
  
  Opsummeringsdata$DPSD_Proces[is.na(Opsummeringsdata$DPSD_Proces)] <-
  "(ikke angivet)"
  
  Opsummeringsdata$DPSD_Problem[is.na(Opsummeringsdata$DPSD_Problem)] <-
  "(ikke angivet)"

  
  # Enkeltsager, forberedelse af data
  
  forberedte_data <- as_tibble(d) %>%
  filter(!duplicated(Sagsnummer)) %>%
  select(
  Sagsnummer,
  Sagsstatus,
  Sagstype,
  `Hændelsesdato`,
  `Hændelsestid` = `Hændelsetid`,
  `Hændelsessted`,
  Involveret_lokationsnavn,
  Opret_dato,
  Sagsafslutning_dato,
  Afdeling,
  Afdeling.y,
  Alvorlighed,
  DPSD_Hovedgruppe,
  DPSD_Proces,
  DPSD_Problem,
  `Hændelsesbeskrivelse`,
  Konsekvens,
  Forslag_til_forebyggelse,
  `Beskrivelse_af_sagsopfølgning`,
  Titel
  ) %>%
  arrange(`Hændelsessted`)
  
  forberedte_data$`Hændelsesdato` <-
  format(as.Date(forberedte_data$`Hændelsesdato`), "%d. %B %Y")
  forberedte_data$`Opret_dato` <-
  format(as.Date(forberedte_data$`Opret_dato`), "%d. %B %Y")
  #forberedte_data$`Afsluttet_dato` <-
  #format(as.Date(forberedte_data$`Afsluttet_dato`), "%d. %B %Y")
  forberedte_data$`Sagsafslutning_dato` <-
  format(as.Date(forberedte_data$`Sagsafslutning_dato`), "%d. %B %Y")
  
  forberedte_data$`Hændelsestid` <-
  format(strptime(forberedte_data$`Hændelsestid`, "%H:%M"), "%H:%M")
  
  forberedte_data$`Hændelsessted` <-
  gsub(
  'Region Hovedstaden Bispebjerg og Frederiksberg Hospitaler |Region Hovedstaden Bispebjerg Hospital ',
  '',
  forberedte_data$`Hændelsessted`
  )
  forberedte_data$`Hændelsessted` <-
  str_replace_all(forberedte_data$`Hændelsessted`,
  '[[~!@#$%^&*(){}\\ +:<>?./;=]]',
  ' ')
  
  forberedte_data <- as_tibble(lapply(forberedte_data, tilfoejTom))
  forberedte_data[is.na(forberedte_data)] <- "(ikke angivet)"
  
  #åbne sager
  out_aabne <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Åben", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
 
   #Videresendte sager
  out_videresendte <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Videresendt", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #Accepterede sager
  out_accepterede <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Accepteret", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #Oprettede sager
  out_oprettede <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus == "Oprettet", as.Date(Opret_dato, format = "%d. %B %Y") > as.Date(seneste_90dage_start_dato))
  
  #lukkede sager
  out_lukkede_sager <- forberedte_data %>%
  filter(
  Afdeling.y == afd,
  Sagsstatus == "Lukket",
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") >= seneste_maaned_start_dato,
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") < seneste_maaned_slut_dato
  )
  
  #Sager ældre end
  out_aeldre_sager <- forberedte_data %>%
  filter(Afdeling.y == afd, Sagsstatus != "Lukket", as.Date(Opret_dato, format = "%d. %B %Y") < as.Date(seneste_90dage_start_dato))
  
  #forslag til forebyggelse
  out_forslag_til_forebyggelse <- forberedte_data %>%
  filter(
  Sagsstatus == "Lukket",
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") >= seneste_maaned_start_dato,
  as.Date(`Sagsafslutning_dato`, format = "%d. %B %Y") < seneste_maaned_slut_dato
  ) %>%
  select(DPSD_Hovedgruppe, DPSD_Proces, DPSD_Problem,
  Titel,
  Forslag_til_forebyggelse)
  
  #involveret lokation
  involveret <- d %>%
  filter(
  !duplicated(Sagsnummer) &
  (
  Opret_dato >= seneste_aar_start_dato &
  Opret_dato < seneste_aar_slut_dato
  ) & Sagsstatus == "Lukket" & Sagstype == "Sundhedsfaglig UTH 2"
  )
  
```

```{r Wordcloud, echo=FALSE, fig.height=7, fig.width=7, message=FALSE, warning=FALSE, dpi = 300}

Afd_words_this <- Afd_words %>%
  filter(Afdeling.y == afd) %>%
  top_n(100)
  
  wordcloud::wordcloud(
  Afd_words_this$word,
  freq = Afd_words_this$freq,
  max.words = 100,
  scale = c(4, .7),
  use.r.layout = FALSE,
  colors = hosp_blaa,
  rot.per = .3,
  random.order = FALSE
  )

```


# Forord
Dette er 1. delrapport af 2 for utilsigtede hændelser i `r format(Sys.Date()-months(1), "%B %Y")` i `r afd`.
Rapporten har følgende opbygning:

* Opsummerede data om utilsigtede hændelser fra `r format(seneste_aar_start_dato, "%B %Y")` til og med `r format(seneste_aar_slut_dato - months(1), "%B %Y")` (`r count(Opsummeringsdata)` sager i alt)
* Organisatorisk læring


Forsideillustrationen er dannet af de 100 vigtigste ord i hændelsesbeskrivelserne fra `r afd``r #format(as.Date(cut(Sys.Date(),'month'))-years(1), "%B %Y")` fra `r format(seneste_aar_slut_dato - months(1), "%B %Y")`.

Tip til læsning på skærm: Vælg "Vis" i MS Word, og sæt hak ved "Navigationsrude" for en navigerbar indholdsfortegnelse.

Tip til læsning på papir: Vælg "Referencer" i MS Word før udskrivning, og indsæt "Indholdsfortegnelse". "Automatisk Tabel 1" og "Automatisk Tabel 2" er lige gode. Desværre er det af tekniske årsager ikke muligt at generere indholdsfortegnelsen når rapporten dannes.

Data til denne rapport er trukket fra DPSD2 den `r format(data_trukket, "%d. %B %Y")`.

Spørgsmål, kommentarer, idéer og ønsker til forbedring af denne rapport modtages gerne, og bedes sendt til johan.reventlow@regionh.dk




Mia Hartley, Risikomanager

Afdeling for Kvalitet og Uddannelse
Bispebjerg og Frederiksberg Hospital

`r format(Sys.Date(), '%e %B %Y')`


# Opsummerede data om utilsigtede hændelser fra `r format(seneste_aar_start_dato, "%B %Y")` til `r format(seneste_aar_slut_dato - months(1), "%B %Y")` (`r count(Opsummeringsdata)` sager i alt)

## UTH'er fordelt efter alvorlighed
```{r Alvorlighed, echo=FALSE, fig.height=5, fig.width=7, message=FALSE, warning=FALSE, dpi = 300}
# alvor <- Opsummeringsdata %>% 
#   filter(Opret_dato > as.Date(cut(Sys.Date(), 'month')) - months(3)) %>% 
#   group_by(Alvorlighed) %>%
#   summarise(Antal=n()) %>%
#   arrange(Antal)
# 
# alvor$Alvorlighed <- factor(alvor$Alvorlighed, levels = c('Ingen skade', 'Mild', 'Moderat', 'Alvorlig', 'Dødelig', '', '(ikke angivet)'))
# 
# ggplot(data=alvor, aes(x=Alvorlighed, y=Antal)) +
#   geom_bar(colour="black", stat="identity") +
#   labs(x="Alvorlighed",y="Antal sager")+
#   scale_x_discrete(labels = function(x) stringr::str_wrap(x, width = 10))+
#   labs(title = "Utilsigtede hændelser fordelt efter alvorlighed", subtitle=afd, caption = paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', count(Opsummeringsdata), ' sager i alt.', sep="", collapse=""))+
#   theme_minimal()



alvor <- Opsummeringsdata %>%
  mutate(Status = ifelse(Sagsstatus != "Lukket", "Uafsluttede sager", "Afsluttede sager")) %>% 
  group_by(Status, Alvorlighed) %>%
  summarise(Antal=n()) %>%
  arrange(Status, Antal)

alvor$Alvorlighed <- factor(alvor$Alvorlighed, levels = c('Ingen skade', 'Mild', 'Moderat', 'Alvorlig', 'Dødelig', '', '(ikke angivet)'))

alvor$Status <- factor(alvor$Status, levels = c("Uafsluttede sager", "Afsluttede sager"))

ggplot(data=alvor, aes(x=Alvorlighed, y=Antal, fill=Status )) +
  geom_bar(position="stack",  stat="identity") +
  labs(x="Alvorlighed",y="Antal sager")+
  scale_x_discrete(labels = function(x) stringr::str_wrap(x, width = 10))+
  labs(title = "Utilsigtede hændelser fordelt efter alvorlighed", subtitle=afd, caption = paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', count(Opsummeringsdata), ' sager i alt.', sep="", collapse=""))+
  theme_minimal()+
  theme(legend.position = "top", legend.title=element_blank())+
  scale_fill_grey(start=0.7, end=0.2)



```

## UTH'er fordelt efter DPSD hovedgruppe
```{r DPSD_Hovedgruppe, echo=FALSE, fig.height=7, fig.width=7, message=FALSE, warning=FALSE, dpi = 300}
DPSD <- Opsummeringsdata %>% 
  group_by(DPSD_Hovedgruppe) %>%
  summarise(Antal=n()) %>%
  arrange(Antal) %>% 
  transform(DPSD_Hovedgruppe = reorder(DPSD_Hovedgruppe, seq(Antal)))


ggplot(data=DPSD, aes(x=`DPSD_Hovedgruppe`, y=Antal)) +
  geom_bar(colour="black", stat="identity", width=0.7) +
  scale_x_discrete(labels = function(x) stringr::str_wrap(x, width = 35))+
  scale_fill_discrete(guide="none")+
  labs(x="",y="Antal sager")+
  #scale_y_discrete(labels = function(x) stringr::str_wrap(x, width = 40 ))+
  coord_flip()+
  labs(title = "Utilsigtede hændelser fordelt efter DPSD hovedgruppe", subtitle=afd, caption = paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', count(Opsummeringsdata), ' sager i alt.', sep="", collapse=""))+
  theme_minimal() +
  theme(axis.text.y = element_text(size=9))

```

## UTH'er fordelt efter DPSD hovedgruppe og alvorlighed
```{r DPSD_Alvorlighed, echo=FALSE, message=TRUE, warning=TRUE,  fig.width=7, fig.height=6, dpi = 300}
DPSD_Alvor <- Opsummeringsdata %>% 
  group_by(DPSD_Hovedgruppe, Alvorlighed)  %>% 
  summarise(Antal=n()) %>%
  arrange(desc(DPSD_Hovedgruppe)) %>%
  transform(DPSD_Hovedgruppe = reorder(DPSD_Hovedgruppe, seq(Antal)))

DPSD_Alvor$Alvorlighed <- factor(DPSD_Alvor$Alvorlighed, levels = c('(ikke angivet)', 'Ingen skade', 'Mild', 'Moderat', 'Alvorlig', 'Dødelig', '' ))

ggplot(data=DPSD_Alvor, aes(y=DPSD_Hovedgruppe, x=Alvorlighed )) +
  geom_point(stat="identity", aes(size=DPSD_Alvor$Antal), alpha=1) + 
  labs(x="Alvorlighed",y="DPSD hovedgruppe", size="Antal")+
  scale_y_discrete(labels = function(x) stringr::str_wrap(x, width = 40 )) +
  labs(title = "Utilsigtede hændelser fordelt efter DPSD \nhovedgruppe og alvorlighed", subtitle=afd, caption = paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', count(Opsummeringsdata), ' sager i alt.', sep="", collapse="")) +
    scale_x_discrete(labels = function(x) stringr::str_wrap(x, width = 10 )) +
  theme(axis.text.y = element_text(size=9)) +
  theme_minimal()


```

> sideskift

## UTH'er fordelt efter DPSD proces og DPSD problem
Nedenstående grafer viser antallet af utilsigtede hændelser der er opstået i forbindelse med udførelsen af bestemte arbejdsgange / processer (DPSD Proces) krydset med hvilken problematik (DPSD Problem) der var tale om. Graferne vises for DPSD hovedgrupper der har mere end 5 sager gennem det seneste år.
```{r DPSD_proces, echo=FALSE, message=TRUE, warning=TRUE, fig.height=7, fig.width=7, dpi = 300}
#fig.height=9, fig.width=8 
proces_problem <- Opsummeringsdata %>% 
  group_by(DPSD_Hovedgruppe, DPSD_Proces, DPSD_Problem)  %>% 
  summarise(Antal=n()) %>%
  arrange(DPSD_Proces, DPSD_Problem) %>%
  transform(DPSD_Proces = reorder(DPSD_Proces, seq(Antal))) %>%   ungroup()

#, DPSD_Problem = reorder(DPSD_Problem, seq(Antal))


#DPSD_Alvor$Alvorlighed <- factor(DPSD_Alvor$Alvorlighed, levels = c('(ikke angivet)', 'Ingen skade', 'Mild', 'Moderat', 'Alvorlig', 'Dødelig', '' ))
for (i in sort(unique(proces_problem$DPSD_Hovedgruppe))) {
  proces_problem1 <- proces_problem  %>% 
    filter(DPSD_Hovedgruppe == i) %>% 
    arrange(DPSD_Problem)
  
proces_problem1  <- within(proces_problem1,
                         DPSD_Problem <- ordered(DPSD_Problem, levels = rev(sort(unique(DPSD_Problem)))))
  
titel_text <- paste(strwrap(paste("Utilsigtede hændelser i DPSD hovedgruppe '", i , "' opgjort efter DPSD proces og DPSD problem", sep=""), width= 50), collapse="\n")

caption_text <-  paste(strwrap(paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', sum(proces_problem1$Antal), ' sager i alt i denne DPSD hovedgruppe.', sep="", collapse=""), width= 55), collapse="\n")

proportion <- length(unique(proces_problem1$DPSD_Problem))/nlevels(droplevels(proces_problem1$DPSD_Proces))
  
  
if ((sum(proces_problem1$Antal) >= 5) & count(proces_problem1) > 2) {
  
print(ggplot(proces_problem1, aes(x=DPSD_Problem, y=DPSD_Proces)) +
  geom_point(stat="identity", aes(size=proces_problem1$Antal), alpha=1) + 
  labs(y="Proces",x="Problem", size="Antal")+
  scale_y_discrete(labels = function(x) stringr::str_wrap(x, width = 30 )) +
  scale_x_discrete(labels = function(x) stringr::str_wrap(x, width = 50 )) +
  labs(title = titel_text, caption = caption_text) +
  theme_minimal()+
    coord_fixed(ratio = proportion, expand=TRUE)+
  theme(axis.text.y = element_text(size=9)) +
  theme(axis.text.x = element_text(size=9, angle = 90, hjust = 1, vjust=0.5)))
  
  
  
  # +
  # theme(axis.text.x = element_text(angle = 0, hjust = 1, , vjust=0, size=9)))

  # +
  # theme(axis.text.y = element_text(size=9)) +
  # theme(axis.text.x = element_text(size=9)))
  
}
cat('\n')
}

```

> sideskift



## UTH sagsbehandlingstid
Grafen viser sagsbehandlingstiden i antal dage, og er opgjort efter den måned hvor sagen er afsluttet. Endnu ikke afsluttede sager (`r count(filter(Opsummeringsdata, Sagsstatus != "Lukket"))` stk. ) er i dette regnestykke sat til at have sagsafslutning `r format(seneste_maaned_slut_dato - days(1), "%d. %B %Y")`, og tæller således med i seneste måned. Sagsbehandlingstiden er udregnet som antallet af dage mellem sagsoprettelse og sagsafslutning.

```{r Sagsbehandlingstid, echo=FALSE, fig.height=9.5, fig.width=7, message=FALSE, warning=FALSE, dpi = 300}
DPSD_Sagsbehandlingstid <- Opsummeringsdata

DPSD_Sagsbehandlingstid$Sagsafslutning_dato[is.na(DPSD_Sagsbehandlingstid$Sagsafslutning_dato)] <- as.Date(cut(Sys.Date(), "month")) -1

DPSD_Gns_tid <- DPSD_Sagsbehandlingstid %>% 
  mutate(Sagsbehandlingstid = difftime(DPSD_Sagsbehandlingstid$Sagsafslutning_dato, DPSD_Sagsbehandlingstid$Opret_dato, units = "days"), 
         mo=as.Date(cut(Sagsafslutning_dato, "month"))) %>% 
  filter(Sagsbehandlingstid > 0, mo < as.Date(cut(Sys.Date(), "month"))) %>% 
  group_by(mo) %>% 
  summarise("Nedre kvartil" = quantile(Sagsbehandlingstid, probs=0.25),
            "Median sagsbehandlingstid" = quantile(Sagsbehandlingstid, probs=0.50),
            "Øvre kvartil" = quantile(Sagsbehandlingstid, probs=0.75),
            "Gennemsnitlig sagsbehandligstid" = mean(Sagsbehandlingstid), 
            "Korteste sagsbehandlingstid" = quantile(Sagsbehandlingstid, probs=0),
            "Længste sagsbehandlingstid" = quantile(Sagsbehandlingstid, probs=1)) %>% 
  gather(key=type, value=percentil, -mo )

ggplot(data=transform(DPSD_Gns_tid,
                      type=factor(type,levels=c("Korteste sagsbehandlingstid","Længste sagsbehandlingstid","Nedre kvartil", "Øvre kvartil", "Gennemsnitlig sagsbehandligstid", "Median sagsbehandlingstid"))), 
       aes(y=percentil, x=mo )) +
  geom_bar(colour="black", stat="identity", width=20) + 
  geom_line(colour="grey", size = 1, aes(x = mo, y = 90)) +
  labs(x="",y="Antal dage", size="") +
  facet_wrap(~ type, ncol=2, scales = "free_x", as.table=TRUE)+
  labs(title = "Utilsigtede hændelser, sagsbehandlingstid", subtitle=afd, caption = paste('Data: Utilsigtede hændelser fra ',format(seneste_aar_start_dato, "%b %Y"), ' til ', format(seneste_aar_slut_dato - months(1), "%b %Y"), ', ', count(Opsummeringsdata), ' sager i alt.', sep="", collapse=""))+
  theme_minimal()+ 
  theme(panel.spacing.y = unit(3, "lines"))+
  scale_fill_grey(start = 0.2, end = 0.8, na.value = "red") 


```
  
  
  
Læsevejledning: 

* 'Nedre kvartil' og 'Øvre kvartil' repræsenterer den observation, hvor alle observationer der er mindre end den, udgør hhv. 25% og 75% af samtlige observationer 
* 'Gennemsnit' og 'Median' angiver den normale sagsbehandlingstid. Medianen ikke er følsom for ekstreme observationer, hvorimod gennemsnittet er.
* Den lovgivningsbestemte maksimumgrænse på 90 dages sagsbehandlingstid er indikeret med lysegrå vandret streg.


```{r Sagsbehandlingstid_aabne, echo=FALSE, fig.height=4, fig.width=7, message=FALSE, warning=FALSE, , results='asis', dpi = 300}
# 
# DPSD_Sagsbehandlingstid <- Opsummeringsdata %>% 
#   filter(Sagsstatus != "Lukket")
# 
#  DPSD_Sagsbehandlingstid$Sagsafslutning_dato[is.na(DPSD_Sagsbehandlingstid$Sagsafslutning_dato)] <- as.Date(cut(Sys.Date(), "month")) 
# 
# DPSD_alder <- DPSD_Sagsbehandlingstid %>% 
#   mutate(alder = difftime(DPSD_Sagsbehandlingstid$Sagsafslutning_dato, DPSD_Sagsbehandlingstid$Opret_dato, units = "days"))
# 
# if (count(DPSD_alder) > 10) {
# cat('### Uafsluttede sager, sagsbehandlingstid')
# 
# print(ggplot(data=DPSD_alder, aes(x=as.numeric(alder), y=1))+
#   geom_line(colour="grey", size = 1, aes(y = alder, x = 90)) +
#   annotate(geom = "text", x = 140, y = 475, label = "90 dages frist")+
#   geom_dotplot(dotsize = 0.5, 
#                binwidth = 7, 
#                binaxis = "x", 
#                method="histodot",
#                drop = TRUE,
#                stackratio = 1.5)+ 
#   labs(y="Sager",x="Antal dage", size="") +
#   labs(title = "Uafsluttede utilsigtede hændelser, sagsbehandlingstid",  caption = paste('Data: Uafsluttede utilsigtede hændelser pr. ', format( as.POSIXct(cut(Sys.Date(), "month")), "%d. %b %Y"), ', ', count(filter(Opsummeringsdata, Sagsstatus != "Lukket")), ' sager i alt.', sep="", collapse=""))+
#   scale_y_continuous( breaks = NULL)+
#   theme_minimal())
# }

```


# Organisatorisk læring
Hændelsesbeskrivelser, beskrivelser af konsekvenser og forslag til forebyggelse fra de indrapporterede utilsigtede hændelser er vigtige kilder til arbejdet med løbende forbedring af den behandling som Bispebjerg og Frederiksberg Hospital tilbyder patienter og pårørende.

For at styrke og understøtte den tværorganisatoriske erfaringsopsamling og læring præsenteres i det følgende et samlet overblik over alle forslag til forebyggelse af fremtidige utilsigtede hændelser, samt opgørelser over ord fra hændelsesbeskrivelserne som statistisk set fremstår som vigtige, sorteret efter DPSD hovedgruppe og alvorlighed. Datagrundlaget er alle rapporterede hændelser fra hele hospitalet som er lukket i `r format(seneste_aar_slut_dato - months(1), "%B %Y")`. 

## Forslag til forebyggelse `r format(seneste_aar_slut_dato - months(1), "%B %Y")`
```{r kable_forebyggelse, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

out <- out_forslag_til_forebyggelse %>% 
    arrange(DPSD_Hovedgruppe, DPSD_Proces, DPSD_Problem) %>% 
  group_by(DPSD_Hovedgruppe)

for (i in unique(out$DPSD_Hovedgruppe)){
  gruppe <- out %>%
    ungroup() %>%
    filter(DPSD_Hovedgruppe == i)
  gruppe1 <- gruppe %>%
    rename(`DPSD Proces` = DPSD_Proces, `DPSD Problem` = DPSD_Problem, `Forslag til forebyggelse` = Forslag_til_forebyggelse, `UTH titel` = Titel) %>%
    select(-DPSD_Hovedgruppe)
  cat('\n')
  cat('### ', i)
  gruppe1 %>%
    kable() %>%
    print()
  cat('\n')
}


# out <- out_forslag_til_forebyggelse %>% 
#     arrange(DPSD_Hovedgruppe, DPSD_Problem, DPSD_Proces) %>% 
#   group_by(DPSD_Hovedgruppe)
# for (i in sort(unique(out$DPSD_Hovedgruppe))){
#   gruppe <- out %>%
#     filter(DPSD_Hovedgruppe == i) %>% 
#   ungroup() 
#   
#   cat('\n')
#   cat('### ', i)
#   for (j in sort(unique(gruppe$DPSD_Proces))){
#     cat('\n')
#     cat('##### Problemområde: ', j)
#     gruppe1 <- gruppe %>%
#       filter(DPSD_Proces == j) %>% 
#       rename(`Forslag til forebyggelse` = Forslag_til_forebyggelse, 
#              `UTH titel` = Titel, Problematik = DPSD_Problem) %>%
#       select(-DPSD_Hovedgruppe, -DPSD_Proces)
#     
#     gruppe1 %>%
#       kable() %>%
#       print()
#     cat('\n')
#   }
# }


```

## Opgørelse af vigtige ord i `r format(seneste_aar_slut_dato - months(1), "%B %Y")`
For at understøtte tolkningen af den store mængde kvalitative data som de utilsigtede hændelser indeholder, er hændelsesbeskrivelser og beskrivelser af konsekvenser bearbejdet med en teknik som kaldes "Term Frequency–Inverse Document Frequency". TF-IDF finder de ord som optræder hyppigst i en given kategori (fx alvorlige hændelser), og disse justeres i forhold til de hyppigste ord i den samlede tekstmængde, for at finde de ord som er særlige i den enkelte kategori.  

### Vigtigste ord sorteret efter alvorlighed
```{r Alvor_ord, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

Alvor_words1 <- Alvor_words %>%
  select(-total) %>%
  arrange(Alvorlighed, desc(freq)) %>% 
  group_by(Alvorlighed= as.character(Alvorlighed)) %>% 
  top_n(25) %>% 
  summarise(`Vigtigste ord` = paste(word, collapse = ", "))  

Alvor_words1$Alvorlighed <- factor(Alvor_words1$Alvorlighed, levels = c('Ingen skade', 'Mild', 'Moderat', 'Alvorlig', 'Dødelig', '' ))


Alvor_words1 %>% 
arrange(Alvorlighed) %>% 
  filter(Alvorlighed != "Dødelig") %>% 
    kable()

```  
  
Dødelige hændelser indgår ikke i analysen.

### Vigtigste ord sorteret efter DPSD hovedgruppe
```{r DPSD_ord, echo=FALSE, message=FALSE, warning=FALSE, results='asis'}

DPSD_words %>%
  select(-total) %>%
  arrange(DPSD_Hovedgruppe, desc(freq)) %>% 
    filter(DPSD_Hovedgruppe != "Selvskade og selvmord") %>% 
  group_by(Hovedgruppe= DPSD_Hovedgruppe) %>% 
  select(word, n, tf_idf) %>% 
  top_n(25) %>% 
  summarise(`Vigtigste ord` = paste(word, collapse = ", ")) %>% 
#  arrange(Hovedgruppe) %>% 

    kable()

```
  
  
  
Hovedgruppen "Selvskade og selvmord" indgår ikke i analysen.
